# Using the server's API

## GET /
Show a basic information page about the server

## GET /test
Test the connection.

Returns '200: Test OK' when successful.

Returns '200: Test SSL OK' when successfully connected using a client
certificate. An error message is returned otherwise.

## POST /register
Register your new country node using the connecting IP address and a provided
ISO 3166 alpha-2 country code.

A JSON formatted body must be provided containing the following:
```
{ "country": "XX" }
```
Where `XX` must be replaced by the aforementioned country code you wish to use.

A request to register from your server's command line can be made with `curl`:
```
$ curl https://[NOOB IP]:[NOOB PORT]/register --key [private key] --cert [client certificate] -k -H "Content-type: application/json" -d '{ "country": "XX" }'
```

Registration only has to be performed once. No 2 IP addresses may use the same
country code.

The country code associated with your IP address can be changed by performing a
new request. The old country code will become available again.

The URL where requests will be sent to your country's node is `https://[COUNTRY NODE IP]:5443`. The port can't be changed.

Requests are only processed if a valid client certificate is provided.

## POST /balance
Send a balance request to another country node.

A JSON formatted body must be provided containing at least the following:
```
{
	"header": {
		"receiveCountry": "XX",
		"receiveBankName", "XXXX"
	},
	"body": {}
}
```
Where `XX` must be replaced by a valid 2 character country code that is
registered to the N.O.O.B. The value of receiveBankName (`XXXX`) is not used by
the N.O.O.B., but is checked to be of 4 characters.

HTTP Error 5xx is returned in case a syntax error is encountered, the connection
with the country node failed or if N.O.O.B. experienced an internal error. See
the response body for more information. Message bodies from the N.O.O.B. are
always prefixed with 'NOOB:'.

HTTP Error 501 is returned if the country node doesn't implement the requested
endpoint.

(Almost) all other HTTP reponse codes are directly returned from the country
node, this is not guaranteed however. The response body of the country node is
also returned.

Requests are only processed if a valid client certificate is provided.

## POST /withdraw
Send a withdrawal request to another country node.

See POST /balance for more information

## POST /transfer
Send a withdrawal request to another country node.

See POST /balance for more information


# Running the server using pm2

It's recommended to use pm2 for process management. noob2 doesn't come with
pm2, global installation is preferred to minimize unnecessary dependencies.

Install pm2:
```
$ npm install -g pm2
```

To run in production mode, export the following environment variable or prepend
it to the initial `pm2 start` command:
```
$ export NODE_ENV=production
```

Then run the server as follows:
```
$ pm2 start --name noob server.js
```

The server can now be controlled in the background like so:
```
$ pm2 {start, restart, stop} noob
```

To view the server logs, run:
```
$ pm2 log noob
```
