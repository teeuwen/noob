/*
 * MIT License
 *
 * Copyright (C) 2021 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

const countrySchema = __dirname + '/landSchema.json';
const countries = require(countrySchema)	/* country code to IP mappings */
const express	= require('express');		/* for API endpoints */
const https	= require('https');		/* for HTTP over SSL */
const fs	= require('fs');		/* for loading in certificates from fs */
const got	= require('got');		/* for sending API calls to country node */
		  require('log-timestamp');	/* for displaying the timestamp on the console output */

const app = express();

/* instruct express to parse application/json */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/* create a new server */
const hostname = process.env.HOSTNAME || '127.0.0.1';
const port = process.env.PORT || 5443;

const https_options = {
	key: fs.readFileSync(__dirname + '/certs/intermediates/noob-server/private/server.key'),
	cert: fs.readFileSync(__dirname + '/certs/intermediates/noob-server/certs/server-chain.crt'),
	ca: [ fs.readFileSync(__dirname + '/certs/intermediates/country/certs/ca-chain.crt') ],
	requestCert: true,
	rejectUnauthorized: false
};
const server = https.createServer(https_options, app);

/* Check if the JSON contains all that is required and return an error message otherwise */
function checkHeaders(body) {
	/* check if the header is correct */
	if (!body['header'])
		return 'Syntax error: header is missing';
	if (!body['header']['receiveCountry'])
		return 'Syntax error: receiveCountry is missing';
	if (body['header']['receiveCountry'].length != 2)
		return 'Syntax error: receiveCountry is of invalid length, should be 2 characters';
	if (!body['header']['receiveBankName'])
		return 'Syntax error: receiveBankName is missing';
	if (body['header']['receiveBankName'].length != 4)
		return 'Syntax error: receiveBankName is of invalid length, should be 4 characters';

	/* check if the body exists */
	if (!body['body'])
		return 'Syntax error: body is missing';

	return true;
}

/* Get the IP of a country node by ISO 3166 alpha-2 country code */
function getCountryIp(countryCode) {
	if (!countries[countryCode])
		return false;

	return countries[countryCode];
}

/* Generic endpoint for all requests */
async function handleRequest(req, res) {
	const srcIp = req.connection.remoteAddress;

	/* check if the client is authenticated using their client certificate */
	if (!req.client.authorized) {
		const cert = req.connection.getPeerCertificate();

		console.warn(`${srcIp}: Unauthorized`);

		if (cert.subject)
			return res.status(403).send(`NOOB: Invalid issuer: ${cert.issuer.CN}`);
		else
			return res.status(401).send('NOOB: No client certificate provided');
	}

	/* check if the content-type is JSON */
	if (!req.is('application/json')) {
		console.warn(`${srcIp}: Incorrect Content-type`);
		return res.status(400).send('NOOB: Incorrect Content-type');
	}

	/* parse the JSON headers */
	if ((ret = checkHeaders(req.body)) !== true) {
		console.warn(`${srcIp}: ${ret}`);
		return res.status(400).send(`NOOB: ${ret}`);
	}

	const receiveCountry = req.body['header']['receiveCountry'];
	const receiveBankName = req.body['header']['receiveBankName'];

	/* get the country IP */
	let destIp;
	if (!(destIp = getCountryIp(receiveCountry))) {
		return res.status(404).send(`NOOB: Country code '${receiveCountry}' is unknown to N.O.O.B.`);
	}

	console.info(`${srcIp}: ${req.originalUrl} request to ${receiveBankName} in ${receiveCountry} (${destIp})`);

	/* forward the request to the country node, retry once in case of failure */
	let response;
	try {
		response = await got.post(`https://${destIp}${req.originalUrl}`, {
			https: {
				key: https_options.key,
				certificate: https_options.cert,
				certificateAuthority: https_options.ca,
				rejectUnauthorized: true
			},
			json: req.body['body'],
			/* make sure errors are sent back to the requesting country node and not thrown */
			throwHttpErrors: false
		});
	} catch (err) {
		console.warn(`${srcIp}: ${req.originalUrl} ERROR from ${receiveBankName} in ${receiveCountry} (${destIp}): ${err}`);

		/* handle connection errors to the country node */
		switch (err.code) {
		case 'ECONNRESET':
			return res.status(502).send('NOOB: The country node closed their connection forcibly');
		case 'ECONNREFUSED':
			return res.status(503).send('NOOB: The connection was refused by the country node');
		case 'ETIMEDOUT':
			return res.status(504).send('NOOB: The connection to the country node timed out');
		default:
			return res.status(500).send('NOOB: Error whilst communicating with country node: ' + err);
		}
	}

	/* handle '404 Not Found' from the country node, send '501 Not Implemented' back */
	if (response.statusCode == 404) {
		console.warn(`${srcIp}: ${receiveCountry} (${destIp}) doesn't implement ${req.originalUrl}`);
		return res.status(501).send(`NOOB: This country node doesn't implement ${req.originalUrl}`);
	}

	console.info(`${srcIp}: ${req.originalUrl} response from ${receiveBankName} in ${receiveCountry} (${destIp}): ${response.statusCode}`);
	return res.status(response.statusCode).send(response.body);
}

/* Generic info page */
app.get('/', (req, res) => res.send('N.O.O.B. server'));
app.post('/', (req, res) => res.status(405).send('Use GET instead of POST'));

/* Check connection */
app.get('/test', (req, res) => {
	if (req.client.authorized) {
		return res.send('Test SSL OK');
	} else {
		const cert = req.connection.getPeerCertificate();

		if (cert.subject)
			return res.status(403).send(`NOOB: Invalid issuer: ${cert.issuer.CN}`);
		else
			return res.send('Test OK');
	}
});
app.post('/test', (req, res) => res.status(405).send('Use GET instead of POST'));

/* Register a new country with the server */
app.get('/register', (req, res) => res.status(405).send('Use POST instead of GET'));
app.post('/register', (req, res) => {
	const srcIp = req.connection.remoteAddress;

	/* check if the client is authenticated using their client certificate */
	if (!req.client.authorized) {
		const cert = req.connection.getPeerCertificate();

		console.warn(`${srcIp}: Unauthorized`);

		if (cert.subject)
			return res.status(403).send(`NOOB: Invalid issuer: ${cert.issuer.CN}`);
		else
			return res.status(401).send('NOOB: No client certificate provided');
	}

	/* check if the content-type is JSON */
	if (!req.is('application/json')) {
		console.warn(`${srcIp}: Incorrect Content-type`);
		return res.status(400).send('NOOB: Incorrect Content-type');
	}

	/* check if the country was provided */
	if (!req.body['country']) {
		console.warn(`${srcIp}: Syntax error: country is missing`);
		return res.status(400).send('NOOB: Syntax error: country is missing');
	}

	/* check if country code if of correct length */
	if (req.body['country'].length != 2) {
		console.warn(`${srcIp}: Syntax error: country is of invalid length, should be 2 characters`);
		return res.status(400).send('Syntax error: country is of invalid length, should be 2 characters');
	}

	/* check if this IP is already registered */
	let changed = false;

	for (country in countries) {
		/* don't allow overwriting another country */
		if (country == req.body['country']) {
			if (countries[country].startsWith(srcIp)) {
				console.info(`${srcIp}: Already registered`);
				return res.status(200).send(`NOOB: Already registered`);
			} else {
				console.info(`${srcIp}: Country code already in use`);
				return res.status(200).send(`NOOB: Country code already in use by someone else`);
			}
		}

		if (!countries[country].startsWith(srcIp))
			continue;

		/* if so, change the country to the new one provided */
		delete countries[country];
		changed = country;
	}

	/* add the new country and write the country schema to disk */
	countries[req.body['country']] = srcIp + ':5443';
	fs.writeFile(countrySchema, JSON.stringify(countries), (err) => {
		if (err) {
			console.error(err);
			res.status(500).send(`NOOB: Internal server error`);
			return;
		}

		if (changed) {
			console.warn(`${srcIp}: Changed from ${changed} to ${req.body['country']}`);
			res.status(201).send(`NOOB: Changed from ${changed} to ${req.body['country']}`);
		} else {
			console.info(`${srcIp}: Registered as ${req.body['country']}`);
			res.status(201).send(`NOOB: Registered as ${req.body['country']}`);
		}
	});

	return;
});

/* Send a balance request to an external country node (SSL required) */
app.get('/balance', (req, res) => res.status(405).send('Use POST instead of GET'));
app.post('/balance', handleRequest);

/* Send a withdrawal request to an external country node (SSL required) */
app.get('/withdraw', (req, res) => res.status(405).send('Use POST instead of GET'));
app.post('/withdraw', handleRequest);

/*
 * Send a transfer request to an external country node (SSL required)
 * It's optional to implement this feature
 */
app.get('/transfer', (req, res) => res.status(405).send('Use POST instead of GET'));
app.post('/transfer', handleRequest);

/* Override of 404 HTTP error */
app.use((req, res, next) => res.status(404).send(`NOOB: Invalid endpoint`));

/*
 * Override internal server errors
 * In this case to show that the client has an error in their JSON syntax
 */
app.use((err, req, res, next) => {
	if (err instanceof SyntaxError && err.status === 400 && 'body' in err)
		return res.status(400).send('NOOB: JSON syntax error: ' + err.message);

	console.error(err);

	return res.status(500).send(`NOOB: Internal server error`);
});

/* Start the server */
server.listen(port, hostname, () => {
	console.info(`N.O.O.B. started @ https://${hostname}:${port}/`);
});
